<?php $majors = array("EMPTY"=>"", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
  if (isset($_POST["search"])) {
    session_start();
    $h = $_POST["keyword"];
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <title>Sign Up For New Student</title>
    <style>
        .establish {
         display: flex;
        line-height: 30px;
        margin-top: 8px;
        }
        .box_label {
            width: 140px;
            max-height: 30px;
            max-width: 130px;
             background-color: rgb(102, 143, 255);
            margin-right: 40px;
            text-align: center;
            color: #fff;border: 2px solid #0b67ad;
        }
         
        .infor_text {
            /*margin-top: 18px;*/
            width: 600px;
    }
        .input-text {
            width: 8rem;

          
        }
         .space {
            width: 8rem;
        }
       
         .signup-submit {
        display: flex;
        justify-content: center;
        margin-top: 2rem;
        

    }
     .signup-submit1 {
        display: flex;
        justify-content: right;
        margin-top: 2rem;

    }
      input[type="submit"] {
        margin-top: 0px;
        height: 50px; 
        width: 120px; 
        border-radius: 6px; 
        border: 2px solid rgb(51, 51, 255); 
        background-color: rgb(0, 0, 204); 
        color: #fff; 
        font-size: 15px;
    }
    .title{
        text-align: left;
    }
    </style>
</head>
<body>
        <div  style = "border: 2px solid rgb(26, 50, 255);
        background-color: white;
        padding: 50px 40px 50px 50px;
        position: absolute;
        margin-left: 20rem;
        margin-top: 2%;
        width: 48rem;
    ">
                <div class="establish">
                     <label class="space">
                        Khoa
                        
                    </label>
                    <select id="major" name='major'>
                    <?php
                        foreach ($majors as $key => $value) {
                            echo "<option";
                            echo (isset($_POST['major']) && $_POST['major'] == $key) ? " selected " : "";
                            echo " value='" . $key . "'>" . $value . "</option>";
                        }
                    ?>
                    </select> 
                
                </div>
                <div class="establish">
                     <label class = "space">
                        Từ khóa
                    </label>
                     <input name = "keyword" type="text"  class="input-text" value = <?php echo @$h; ?>>
                    </div>
                <div class=" signup-submit">
                    <button name = "reset" style="border: 2px solid rgb(26, 50, 255);
                                                  padding: 20px 40px;
                                                  border-radius:25px;
                                                  background-color:#5b9bd5; 
                                                  margin-left: 10%;
                                                  color:white;" type="submit">Xóa</button>
                    <button name = "search" style="border: 2px solid rgb(26, 50, 255);
                    border-radius:25px;
                    background-color:#5b9bd5;
                    padding: 20px 40px; 
                    margin-left: 10%;
                    color:white;" type = "submit">Tìm kiếm</button>
                </div>
 
                <div class="" >
                   <label>Số sinh viên tìm thấy: XXX</label>
                   
                </div>
                <div class=" signup-submit1">
                    <a href="register.php"> <input style="background-color:#5b9bd5;"  type="submit" value="Thêm"></a>
                </div>
                <div>
                    
                    <table style="width:100%">
                      <tr>
                        <th class="title">No</th>
                        <th  class="title">Tên Sinh Viên</th>
                        <th  class="title">Khoa</th>
                        <th class="title">Action</th>
                      </tr>
                      <tbody>
                          <tr>
                            <td>1</td>
                            <td>Nguyễn Văn A</td>
                            <td>Khoa học máy tính</td>
                            <td><button type="submit">Sửa</button></td>
                            <td><button type="submit">Xóa</button></td>
                          </tr>
                          <tr>
                            <td>2</td>
                            <td>Trần Thị B</td>
                            <td>Khoa học máy tính</td>
                            <td><button type="submit">Sửa</button></td>
                            <td><button type="submit">Xóa</button></td>
                          </tr>
                          <tr>
                            <td>3</td>
                            <td>Nguyễn Hoàng C</td>
                            <td>Khoa học Vật Liệu</td>
                            <td><button type="submit">Sửa</button></td>
                            <td><button type="submit">Xóa</button></td>
                          </tr>
                          <tr id="4">
                            <td>4</td>
                            <td>Đinh Quang D</td>
                            <td>Khoa học Vật Liệu</td>
                            <td><button name="edit">Sửa</button></td>
                            <td><button name="delete">Xóa</button></td>
                          </tr>
                          <tr><td>
                        </td></tr>
                          
                      </tbody>
                    </table>

                </div>
            
        </div>
</body>
</html>
<script>

  $(document).ready(function(){
    $("button[name='reset']").click(function(){
        $("#major").val('');
        $(".input-text").val('');
      });
  });

</script>